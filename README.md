# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

 This repo is a web scraper which downloads the results of TCS Mock vita for analysis purposes.
 Version-1.0

### How do I get set up? ###

First of all, verify whether or not the required packages are installed on your computer. You may use "installations.txt" file which is included in the repo to do that.

Having that done, the very next step would be straight away running "tcs_mockVita_2_result.py" and watch out for the magic.This script automatically downloads the TCS mockvita result and writes that into "tcs_mock_vita_2_results.csv".

Now that your required csv file is downloaded you may use that for analysis purposes.